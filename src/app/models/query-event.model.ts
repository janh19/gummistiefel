export class QueryEvent {
  area: number;
  id: string;
  length: number;
  si: number;
  start: Date;
}
