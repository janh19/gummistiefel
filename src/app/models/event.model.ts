import {Timeserie} from './timeserie.model';

export class Event {
  area: number;
  id: string;
  length: number;
  si: number;
  start: Date;
  timeseries: Timeserie[];
}
