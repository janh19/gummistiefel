export class Timeserie {
  area: number;
  date: Date;
  index: string;
  lat: number;
  latMax: number;
  lon: number;
  lonMax: number;
  maxPrec: number;
  meanPrec: number;
  si: number;
  size: number;
  stdv: number;
}
