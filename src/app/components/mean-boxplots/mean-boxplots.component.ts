import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import {UtilService} from '../../services/util.service';
import { QueryEvent } from '../../models/query-event.model';
import { interval } from 'rxjs';
declare var Plotly: any;

@Component({
  selector: 'app-mean-boxplots',
  templateUrl: './mean-boxplots.component.html',
  styleUrls: ['./mean-boxplots.component.scss']
})
export class MeanBoxplotsComponent implements OnInit {


  private layout:any = {barmode: 'group'};
  private boxplotid:string = "boxplot";
  private boxplottilte:string = "";

  public selectedDataType = 'area';




  constructor(private dataService: DataService, private utilService: UtilService) { 

 
    
    this.utilService.compareIntervals.subscribe((intervals) => {


      if (intervals.label1 != "" && intervals.label2 != "") {
       
      let y1:number [] =  intervals.interval1.map(value => {
        
        return value[this.selectedDataType];
       
      });

      let y2:number [] =  intervals.interval2.map(value => {
        
        return value[this.selectedDataType];
       
      });

        let trace1 = {
          y: y1,
          marker: {color: '#66b2ff'},
          name: intervals.label1,
          type: "box"

        }

        let trace2 = {
          y: y2,
          marker: {color: 'blue'},
          name: intervals.label2,
          type: "box"      
        }

        let layout:any = {
          title: "Boxplot " + this.boxplottilte
        };        

        Plotly.newPlot(this.boxplotid, [trace1,trace2], layout)

      }

    });
 
    
    


  }



  ngOnInit() {

    this.build_default_boxplot("init","box",this.boxplotid);

    this.utilService.selectedDataType.subscribe((type) => {

      switch (type) {
        case 'length': {
          this.boxplottilte = 'Length';
          this.selectedDataType = type;
          break;
        }
        case 'intensity': {
          this.boxplottilte = 'Intensity';
          this.selectedDataType = "si"
          break;
        }
        case 'area': {
          this.boxplottilte = 'Area';
          this.selectedDataType = type;
          break;
        }
      }

    });



  }
  build_default_boxplot(bname:string, btype:string, bid:string){

    var trace1 = {
      y: [],
      name: bname,
      type: btype
    };

    var trace2 = {
      y: [],
      type: btype,
    };

    var data = [trace1,trace2];

    Plotly.newPlot(bid, data, this.layout);
    
   

  }



}
