import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeanBoxplotsComponent } from './mean-boxplots.component';

describe('MeanBoxplotsComponent', () => {
  let component: MeanBoxplotsComponent;
  let fixture: ComponentFixture<MeanBoxplotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeanBoxplotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeanBoxplotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
