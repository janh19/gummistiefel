import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedComparisonComponent } from './detailed-comparison.component';

describe('DetailedComparisonComponent', () => {
  let component: DetailedComparisonComponent;
  let fixture: ComponentFixture<DetailedComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
