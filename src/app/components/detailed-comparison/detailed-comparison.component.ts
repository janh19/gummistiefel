import {Component, OnInit} from '@angular/core';
import {DataService} from '../../services/data.service';
import {QueryEvent} from '../../models/query-event.model';
import {CompareIntervals, UtilService} from '../../services/util.service';

@Component({
  selector: 'app-detailed-comparison',
  templateUrl: './detailed-comparison.component.html',
  styleUrls: ['./detailed-comparison.component.scss']
})
export class DetailedComparisonComponent implements OnInit {


  private selectedEvents: QueryEvent[];
  private selectedFirst: QueryEvent;
  private selectedSecond: QueryEvent;

  public selectedDataType: string;


  private compareIntervals: CompareIntervals;
  private firstIntervalMaxEvents: QueryEvent[];
  private secondIntervalMaxEvents: QueryEvent[];

  private firstIntervalLabel = '(Bitte wählen)';
  private secondIntervalLabel = '(Bitte wählen)';

  constructor(private dataService: DataService, private utilService: UtilService) {
    this.utilService.selectedDataType.subscribe((datatype) => {
      this.selectedDataType = datatype;
      this.selectedFirst = null;
      this.selectedSecond = null;
      this.selectedEvents = [];
      if (this.compareIntervals !== undefined) {
        this.drawTable();
        this.utilService.changeSelectedEvents(this.selectedEvents);
      }
    });
    this.utilService.compareIntervals.subscribe((data) => {
      if (data.label1 !== '' && data.label2 !== '' && data.interval1 !== [] && data.interval2 !== []) {
        this.selectedFirst = null;
        this.selectedSecond = null;
        this.selectedEvents = [];
        this.compareIntervals = data;
        this.firstIntervalLabel = data.label1;
        this.secondIntervalLabel = data.label2;
        this.drawTable();
        this.utilService.changeSelectedEvents(this.selectedEvents);
      }
    });

  }

  ngOnInit() {

  }

  private drawTable() {

    switch (this.selectedDataType) {
      case 'length': {
        this.firstIntervalMaxEvents = this.compareIntervals.interval1.sort((a, b) => (a.length < b.length) ? 1 : -1).slice(0, 3);
        this.secondIntervalMaxEvents = this.compareIntervals.interval2.sort((a, b) => (a.length < b.length) ? 1 : -1).slice(0, 3);
        break;
      }
      case 'area': {
        this.firstIntervalMaxEvents = this.compareIntervals.interval1.sort((a, b) => (a.area < b.area) ? 1 : -1).slice(0, 3);
        this.secondIntervalMaxEvents = this.compareIntervals.interval2.sort((a, b) => (a.area < b.area) ? 1 : -1).slice(0, 3);
        break;
      }
      case 'intensity': {
        this.firstIntervalMaxEvents = this.compareIntervals.interval1.sort((a, b) => (a.si < b.si) ? 1 : -1).slice(0, 3);
        this.secondIntervalMaxEvents = this.compareIntervals.interval2.sort((a, b) => (a.si < b.si) ? 1 : -1).slice(0, 3);
        break;
      }
    }

    this.selectedEvents = [];
  }

  public selectFirst(event: QueryEvent) {
    if (event != this.selectedFirst) {
      this.selectedEvents = [];
      this.selectedFirst = event;
      this.selectedEvents.push(event);
      if (this.selectedSecond) {
        this.selectedEvents.push(this.selectedSecond);
      }
      this.utilService.changeSelectedEvents(this.selectedEvents);
    }
  }

  public selectSecond(event: QueryEvent) {
    if (event != this.selectedSecond) {
      this.selectedEvents = [];
      this.selectedSecond = event;
      if (this.selectedFirst) {
        this.selectedEvents.push(this.selectedFirst);
      }
      this.selectedEvents.push(event);
      this.utilService.changeSelectedEvents(this.selectedEvents);
    }
  }

  public niceDateString(date: Date): string {
    const options = {year: 'numeric', month: 'long', day: 'numeric'};

    return date.toLocaleDateString('de-DE', options);
  }

  public niceDateStringNumbers(date: Date): string {
    const options = {year: 'numeric', month: 'numeric', day: 'numeric'};

    return date.toLocaleDateString('de-DE', options);
  }

  public roundValues(val: number): number {
    return Math.round(val * 1000) / 1000;
  }
}
