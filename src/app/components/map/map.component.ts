import {Component, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {UtilService} from '../../services/util.service';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  private map: any;

  constructor(private dataService: DataService, private utilService: UtilService) {
  }

  ngOnInit() {

    this.map = L.map('map').setView([48.137154, 11.576124], 5);

    const rainDropBlue = L.icon({
      iconUrl: 'assets/blue_drop.png',

      iconSize: [20, 20], // size of the icon
      iconAnchor: [10, 10], // point of the icon which will correspond to marker's location
    });
    const rainDropRed = L.icon({
      iconUrl: 'assets/light_blue_drop.png',

      iconSize: [20, 20], // size of the icon
      iconAnchor: [10, 10], // point of the icon which will correspond to marker's location
    });

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',

    }).addTo(this.map);

    // subscribe to added events in detailed-comparison-component
    this.utilService.selectedEvents.subscribe((selectedEvents) => {
      this.addSelectedEventsToMap(selectedEvents, rainDropBlue, rainDropRed);
    });
  }

  private async addSelectedEventsToMap(selectedEvents, rainDropBlue, rainDropRed) {
    // remove all layers (and markers) from map
    this.map.eachLayer((layer) => {
      this.map.removeLayer(layer);
    });
    // add tile layer again
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',

    }).addTo(this.map);


    if (selectedEvents.length !== 2) {
      selectedEvents.forEach((selectedEvent) => {
        // get event from server
        this.dataService.getEvent(selectedEvent.id).subscribe((event) => {
          // iterate through timeseries, add a new marker for every timeserie
          if (event.timeseries.length !== 2) {
            console.log(event.timeseries);
            event.timeseries.forEach((timeserie, i) => {
              L.marker([timeserie.lat, timeserie.lon], {
                icon: rainDropRed,
                title: `Event ID: ${event.id}`,
                alt: 'Ich bin Marker 2'
              }).bindPopup(
                `Index: ${i}\n
              Zeitpunkt: ${timeserie.date.toISOString()}\n
              Area: ${timeserie.area}\n
              Intensität: ${timeserie.si}\n
              ID: ${event.id}`
              ).addTo(this.map);
            });
          }
        });
      });
    } else {
      this.dataService.getEvent(selectedEvents[0].id).subscribe((event) => {
        // iterate through timeseries, add a new marker for every timeserie
        if (event.timeseries.length !== 2) {
          event.timeseries.forEach((timeserie, i) => {
            L.marker([timeserie.lat, timeserie.lon], {
              icon: rainDropRed,
              title: `Event ID: ${event.id}`,
              alt: 'Ich bin Marker 2'
            }).bindPopup(
              `Index: ${i}\n
              Zeitpunkt: ${timeserie.date.toISOString()}\n
              Area: ${timeserie.area}\n
              Intensität: ${timeserie.si}\n
              ID: ${event.id}`
            ).addTo(this.map);
          });
        }
      });
      this.dataService.getEvent(selectedEvents[1].id).subscribe((event) => {
        // iterate through timeseries, add a new marker for every timeserie
        if (event.timeseries.length !== 2) {
          event.timeseries.forEach((timeserie, i) => {
            L.marker([timeserie.lat, timeserie.lon], {
              icon: rainDropBlue,
              title: `Event ID: ${event.id}`,
              alt: 'Ich bin Marker 2'
            }).bindPopup(
              `Index: ${i}\n
              Zeitpunkt: ${timeserie.date.toISOString()}\n
              Area: ${timeserie.area}\n
              Intensität: ${timeserie.si}\n
              ID: ${event.id}`
            ).addTo(this.map);
          });
        }
      });
    }
  }
}
