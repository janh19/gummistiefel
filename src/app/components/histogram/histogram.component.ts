import {Component, OnInit} from '@angular/core';
import {Chart} from 'chart.js';
import {DataService} from '../../services/data.service';
import {QueryEvent} from '../../models/query-event.model';
import {DataIntervals, UtilService} from '../../services/util.service';

@Component({
  selector: 'app-histogram',
  templateUrl: './histogram.component.html',
  styleUrls: ['./histogram.component.scss']
})
export class HistogramComponent implements OnInit {

  public chart: Chart;
  public sliderValue = 5;
  private queryEvents: QueryEvent[] = [];

  constructor(private dataService: DataService, private utilService: UtilService) {
    utilService.sliderValue.subscribe((sliderValue) => {
      if (sliderValue !== -1) {
        this.sliderValue = sliderValue;
        this.generateHistogram();
      }
    });
  }

  ngOnInit() {
    this.createChart();
  }

  private inBoxes(years: number, events: QueryEvent[]) {
    const data: DataIntervals[] = [];
    for (let i = 1979; i < 2017; i = i + years) {
      const startDate = new Date(i, 0);
      const endDate = new Date(i + years, 0);
      data.push({
        label: i + '-' + Math.min(i + years, 2017),
        events: events.filter((event) => event.start >= startDate && event.start < endDate)
      });
    }
    this.utilService.changeDataIntervals(data);

    return data;
  }

  private createChart() {
    this.chart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: 'absolute Anzahl von Starkregenereignissen',
          data: [],
          backgroundColor: 'rgba(255, 99, 132, 1)',
        }]
      },
      options: {
        scales: {
          xAxes: [{
            display: false,
            // barPercentage: 1.3,
            ticks: {
              autoSkip: false,
              max: 3,
            }
          }, {
            display: true,
            ticks: {
              autoSkip: false,
              max: 4,
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  generateHistogram() {
    this.queryEvents = this.dataService.getAllEvents();
    const data = this.inBoxes(this.sliderValue, this.queryEvents);
    this.chart.data.datasets[0].data = data.map(value => value.events.length);
    this.chart.data.labels = data.map(value => value.label);
    this.chart.update();
  }
}
