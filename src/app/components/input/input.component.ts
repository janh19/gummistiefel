import {Component, OnInit} from '@angular/core';
import {CompareIntervals, DataIntervals, UtilService} from '../../services/util.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  public val1 = 'area';
  public sliderValue = 5;
  public intervalOptions = [];
  public interval1: string;
  public interval2: string;

  public dataIntervals: DataIntervals[] = [];

  constructor(private utilService: UtilService) {
  }

  ngOnInit() {
    this.utilService.isLoading.subscribe((isLoading) => {
      if (!isLoading) {
        this.utilService.changeSelectedDataType(this.val1);
        this.emitSliderValue();
      }
    });
    this.utilService.dataIntervals.subscribe((dataIntervals) => {
      this.dataIntervals = dataIntervals;
      this.intervalOptions = [];
      this.interval1 = undefined;
      this.interval2 = undefined;
      dataIntervals.forEach((interval) => {
        this.intervalOptions.push({
          label: interval.label,
          value: interval.label
        });
      });
    });
  }

  changeRadioButton() {
    this.utilService.changeSelectedDataType(this.val1);
    this.utilService.changeSliderValue(this.sliderValue);
  }

  emitSliderValue() {
    this.utilService.changeSliderValue(this.sliderValue);
  }

  compareInterval() {
    this.utilService.compareNewIntervals(
      new CompareIntervals(
        this.interval1,
        this.dataIntervals.filter((dataInterval) => dataInterval.label === this.interval1)[0].events,
        this.interval2,
        this.dataIntervals.filter((dataInterval) => dataInterval.label === this.interval2)[0].events,
      )
    );

  }
}
