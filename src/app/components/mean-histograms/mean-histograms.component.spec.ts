import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeanHistogramsComponent } from './mean-histograms.component';

describe('MeanHistogramsComponent', () => {
  let component: MeanHistogramsComponent;
  let fixture: ComponentFixture<MeanHistogramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeanHistogramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeanHistogramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
