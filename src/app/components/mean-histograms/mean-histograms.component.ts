import {Component, OnInit} from '@angular/core';
import {Chart} from 'chart.js';
import {DataService} from '../../services/data.service';
import 'chartjs-chart-box-and-violin-plot/build/Chart.BoxPlot.js';
import {UtilService} from '../../services/util.service';
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'app-mean-histograms',
  templateUrl: './mean-histograms.component.html',
  styleUrls: ['./mean-histograms.component.scss']
})
export class MeanHistogramsComponent implements OnInit {


  private chart: Chart;
  private namedatatype:string = "area"
  private title:string = "area";
  public selectedDataType = 'area';
  public color:string= "green";



  constructor(private dataService: DataService, private utilService: UtilService) {
    this.utilService.dataIntervals.subscribe((dataIntervals) => {
      

      if (dataIntervals.length > 1) {

      let queryEvents = this.dataService.getAllEvents();
    
      let allvalues = queryEvents.map(val => val[this.namedatatype]);
      
          // mean over all time 
        let sum_all = 0;
        for (let i = 0; i < allvalues.length; i++) {
            sum_all += allvalues[i];
        }
        
        let all_mean = sum_all/allvalues.length;

        this.chart.destroy();

        this.createChart(all_mean,this.color);
        
        this.chart.data.datasets[0].label = 'Durchschnitt ' + this.title;
        

        this.chart.data.datasets[0].data = dataIntervals.map(value => {
          let values;
          switch (this.selectedDataType) {
            case 'area': {
              values = value.events.map(val => val.area);     
              break;
            }
            case 'length': {
              values = value.events.map(val => val.length);
              break;
            }
            case 'intensity': {
              values = value.events.map(val => val.si);
              break;
            }
          }

          

          let sum = 0;
          for (let i = 0; i < values.length; i++) {
            sum += values[i];
          }
          return sum / values.length;

        });
        this.chart.data.labels = dataIntervals.map(value => value.label);
        
       // this.chart.options.plugins[0].annotation.annotations[0].value = 4;
        this.chart.update();
        
      }

    });

  }

  ngOnInit(): void {
    this.createChart();
    this.utilService.selectedDataType.subscribe((type) => {
      this.selectedDataType = type;
      switch (type) {
        case 'length': {
          this.title = 'Length';
          this.namedatatype = "length";
          this.color = "blue";
          break;
        }
        case 'intensity': {
          this.title = 'Intensity';
          this.namedatatype = "si";
          this.color = "black";
          break;
        }
        case 'area': {
          this.title = 'Area';
          this.namedatatype = "area";
          this.color ="green";
          break;
        }
      }
      this.chart.data.datasets[0].label = 'Durchschnitt für ' + this.title;
    });
  }


  private createChart(line=0,color="white") {
    this.chart = new Chart('chart', {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          label: '',
          data: [],
          borderColor: color,
          fill: false
        }]
      },

      options: {
        

        annotation: { 
          annotations: [{
            id: "mean",
            type: 'line',
            mode: 'horizontal',
            scaleID: 'y-axis-0',
            value: line,
            borderColor: 'black',
            borderWidth: 1,
            label: {
              position: "left",
              enabled: true,
              content: parseFloat(line.toString().replace(/,/g, '.')).toFixed(4),
            }
          }]
        },
      
      }as ChartOptions,
      plugins: [ChartAnnotation]
    });
  }
}

