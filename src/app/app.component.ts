import {Component, OnInit} from '@angular/core';
import {UtilService} from './services/util.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'visual-analytics';
  loading = true;

  private isShow:boolean = true;

  toggleDisplay() {
    this.isShow = !this.isShow;
  }

  constructor(private loadingService: UtilService) {

  }

  ngOnInit(): void {
    this.loadingService.isLoading.subscribe((isLoading) => this.loading = isLoading);
  }


}
