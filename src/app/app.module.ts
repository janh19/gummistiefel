import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HistogramComponent} from './components/histogram/histogram.component';
import {ButtonModule, DropdownModule, ProgressSpinnerModule, RadioButtonModule, SliderModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {DetailedComparisonComponent} from './components/detailed-comparison/detailed-comparison.component';
import { MeanHistogramsComponent } from './components/mean-histograms/mean-histograms.component';
import { MeanBoxplotsComponent } from './components/mean-boxplots/mean-boxplots.component';
import { MapComponent } from './components/map/map.component';
import { InputComponent } from './components/input/input.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
declare var Plotly: any;

@NgModule({
  declarations: [
    AppComponent,
    HistogramComponent,
    DetailedComparisonComponent,
    MeanHistogramsComponent,
    MeanBoxplotsComponent,
    MapComponent,
    InputComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SliderModule,
    DropdownModule,
    FormsModule,
    ButtonModule,
    ProgressSpinnerModule,
    RadioButtonModule
  ],
  providers: [
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
