import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Event} from '../models/event.model';
import {QueryEvent} from '../models/query-event.model';
import {map} from 'rxjs/operators';
import {UtilService} from './util.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  private allEventsList: QueryEvent[] = [];

  constructor(private http: HttpClient, private loadingService: UtilService) {
    loadingService.changeLoading(true);

    this.loadAllEvents(null, null, null, null).subscribe((values => {
      this.allEventsList = values;
      loadingService.changeLoading(false);
    }));

  }

  public getEvent(eventID: string): Observable<Event> {
    return this.http.get<Event>(`http://rz-vm154.gfz-potsdam.de:8080/highprecip/events/get?id=${eventID}`)
      .pipe(map(event => {
        event.start = new Date(event.start);
        event.timeseries.forEach((timeserie) => {
          timeserie.date = new Date(timeserie.date);
        });
        return event;
      }));

  }

  public getAllEvents() {
    return this.allEventsList;
  }


  private loadAllEvents(
    length: [number, number],
    si: [number, number],
    area: [number, number],
    polygon: [number, number][]): Observable<QueryEvent[]> {

    const basicURL = 'http://rz-vm154.gfz-potsdam.de:8080/highprecip/events/query?';
    let allPolygonsString = '';

    if (polygon) {
      polygon.forEach((point: [number, number]) => {
        allPolygonsString += point[0] + ' ' + point[1] + ',';
      });
      allPolygonsString = allPolygonsString.slice(0, -1);
    }

    const lengthQuery = length ? `subset=length(${length[0]},${length[1]})&` : '';
    const siQuery = si ? `subset=si(${si[0]},${si[1]})&` : '';
    const areaQuery = area ? `subset=area(${area[0]},${area[1]})&` : '';
    const polygonQuery = polygon ? `subset=intersects(POLYGON ((${allPolygonsString})))` : '';

    const url = basicURL + lengthQuery + siQuery + areaQuery + polygonQuery;

    return this.http.get<QueryEvent[]>(url)
      .pipe(map(events => {
        events.forEach(event => {
          event.start = new Date(event.start);
        });
        return events;
      }));
  }
}
