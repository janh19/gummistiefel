import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {QueryEvent} from '../models/query-event.model';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  private loadingSource = new BehaviorSubject(true);
  isLoading = this.loadingSource.asObservable();

  private dataIntervalsSource = new BehaviorSubject([new DataIntervals('', [])]);
  dataIntervals: Observable<DataIntervals[]> = this.dataIntervalsSource.asObservable();

  private selectedEventsSource = new BehaviorSubject([]);
  selectedEvents: Observable<QueryEvent[]> = this.selectedEventsSource.asObservable();

  private selectedDataTypeSource = new BehaviorSubject('area');
  selectedDataType: Observable<string> = this.selectedDataTypeSource.asObservable();

  private sliderValueSource = new BehaviorSubject(-1);
  sliderValue: Observable<number> = this.sliderValueSource.asObservable();

  private compareIntervalsSource = new BehaviorSubject(new CompareIntervals('', [], '', []));
  compareIntervals: Observable<CompareIntervals> = this.compareIntervalsSource.asObservable();

  changeLoading(isLoading: boolean) {
    this.loadingSource.next(isLoading);
  }

  changeDataIntervals(dataIntervals: DataIntervals[]) {
    this.dataIntervalsSource.next(dataIntervals);
  }

  changeSelectedEvents(queryEvents: QueryEvent[]) {
    this.selectedEventsSource.next(queryEvents);
  }

  changeSelectedDataType(type: string) {
    this.selectedDataTypeSource.next(type);
  }

  changeSliderValue(value: number) {
    this.sliderValueSource.next(value);
  }

  compareNewIntervals(value: CompareIntervals) {
    this.compareIntervalsSource.next(value);
  }


}

export class DataIntervals {
  label: string;
  events: QueryEvent[];

  constructor(label: string, events: QueryEvent[]) {
    this.label = label;
    this.events = events;
  }

}

export class CompareIntervals {
  label1: string;
  label2: string;
  interval1: QueryEvent[];
  interval2: QueryEvent[];

  constructor(label1: string, interval1: QueryEvent[], label2: string, interval2: QueryEvent[]) {
    this.label1 = label1;
    this.label2 = label2;
    this.interval1 = interval1;
    this.interval2 = interval2;
  }

}
